#include "ft_select.h"
#include "libft.h"
#include <stdlib.h>
#include <sys/ioctl.h>

/*
 * Affiche la liste si possibilite
 * Renvoie un si la liste c'est affiche,
 * 0 si la taille de la fenetre est trop petite
 * Affiche une partie de la liste si possible
 * Renvoyer le nbr d'elements affiche ????
 */


static void ft_display_name(t_select *name, t_env *env, unsigned int mot,
		unsigned int total)
{
	unsigned int nbr;
	unsigned size;

	ft_putstr(name->color);
	ft_putstr(name->name);
	ft_putstr(COLOR_DEFAULT);
	if (mot == env->width / env->size_max || total  + 1 >= env->size_lst)
		return ;
	nbr = 0;
	size = env->size_max - name->size;
	ft_putchar(' ');
	while (nbr < size)
	{
		ft_putstr(" ");
		++nbr;
	}
}

static unsigned int ft_next_line(t_select **next_line, t_select **tmp,
		unsigned int *nbrlignes, t_env *env)
{
	*next_line = (*next_line)->next;
	*tmp = *next_line;
	//++*nbrlignes;
		ft_putchar('\n');
	return (0);
}

static int ft_nbr_lignes(unsigned int *nbrlignes, t_select *name, t_env *env)
{
	unsigned int mot_line;

	mot_line = name->size / env->width;
	if (name->size % env->width)
		mot_line++;
	if (*nbrlignes + mot_line >= env->height)
	{
		if (name->next != env->end)
		{
			ft_putstr("...");
			return (0);
		}
	}
	*nbrlignes += mot_line;
	return (1);
}

static void	ft_display(t_select *select, unsigned int w_width, t_env *env,
		unsigned int *nbrlignes)
{
	t_select		*tmp;
	t_select		*next_line;
	unsigned int	mot;
	unsigned int	modulo;
	unsigned int	total;

	tmp = select;
	next_line = select;
	mot = 0;
	total = 0;
	while (tmp && *nbrlignes <= env->height )
	{
		if (!ft_nbr_lignes(nbrlignes, tmp, env))
			return ;
		ft_display_name(tmp, env, mot, total);
		if ((modulo = -1) && ++total >= env->size_lst)
			break ;
		while (w_width && ++modulo <= env->size_lst / w_width)
		{
			if ((tmp = tmp->next) && tmp == select)
				mot = w_width;
		}
		if (++mot >= w_width)
			mot = ft_next_line(&next_line, &tmp, nbrlignes, env);
	}
}


int	ft_display_list(t_env *env, int *fd)
{
	unsigned int nbr;
	unsigned int w_width;
	unsigned int nbrlignes;

	if ((*fd = ft_get_size_fenetre(env)) < 0 )
	{
		ft_putendl("ERROR");
		exit(EXIT_FAILURE);
	}

	nbrlignes = 0;
	if (env->size_max)
		ft_display(env->begin, env->width / env->size_max, env, &nbrlignes);
	else
		ft_putendl("heu........");
	if (nbrlignes == 0)
		ft_putstr("...");
	return (nbrlignes);
}
