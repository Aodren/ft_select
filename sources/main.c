#include "ft_select.h"
#include "libft.h"
#include <signal.h>
#include <stdlib.h>
#include <termios.h>
#include <term.h>

/*
 voir les signaux plus tard
void	*handler(unsigned long int sig)
{
	 ft_putstr("\033[?1049h\033[H");

}
*/



///global pour les test;

/*
void	ft_kill_signal(int sig)
{
	tcsetattr(fd, TCSANOW, &termios_old);
	//faire les free
	ft_putstr("\033[?1049l");
	exit(EXIT_SUCCESS);
}
*/

static void ft_check_args(int argc)
{
	if (argc == 1)
		ft_exit("No arg is given, abording...", 1);
}

int	main(int argc, char **argv)
{
	t_env	env;
	int	fd;

	ft_check_args(argc);
	ft_init_env(&env);
	env.begin = ft_init_list(argc, argv, &env);
	ft_putstr("\033[?1049h\033[H");
	ft_init_access_db();
	ft_display_list(&env, &fd); // Renvoie le nbr de lignes imprime
	ft_init_termios(&env, fd);

	char *line = NULL;
	int ret = 0;


	while (get_next_line(0, &line) >= 0)
	{
		ft_manage_term(line, &env);// avoir pur env
		free (line);
		line =  NULL;
	}
	if (line)
		free (line);
	// On remet l'ancienne strcut du termios
	tcsetattr(fd, TCSANOW, env.old_termios);
	return (0);
}
