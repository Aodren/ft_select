#include "ft_select.h"
/*
 * Init de la struct env
 */

void	ft_init_env(t_env *env)
{

	env->begin = (void *)0;
	env->end = (void *)0;
	env->size_max = 0;
//	env->old_termios = (void *)0;
	env->width = 0;
	env->height = 0;
}	
