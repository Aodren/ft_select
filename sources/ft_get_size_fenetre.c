#include "libft.h"
#include "ft_select.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
/*
 * Calcul la taille de la fenetre
 */

static int	ft_open_tty()
{
	int		fd;
	char	*tty_name;

	if ((fd = ttyslot()) < 0)
	{
		return (-1);
	}
	fd = 0;
	if (!isatty(fd))
		return (-1);
	tty_name = ttyname(fd);
	if ((fd = open(tty_name, O_RDONLY)) == -1)
		return (-1);
	else
		return (fd);
}

int	ft_get_size_fenetre(t_env *env)
{
	int fd;
	struct winsize ws;

	if ((fd = ft_open_tty()) == -1)
		ft_putendl("Erreur sur l'open du fd");
	if (ioctl(fd, TIOCGWINSZ, &ws) == -1)
		ft_putendl("Erreur sur ioctl");
	env->width = ws.ws_col;
	env->height = ws.ws_row;
//	close(fd);
	return (fd);
}
