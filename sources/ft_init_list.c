#include "libft.h"
#include "ft_select.h"
#include <unistd.h>


static t_select	*ft_new_elem(char *name, t_select *begin, t_env *env)
{
	t_select *new;

	new = (t_select *)ft_memalloc(sizeof(t_select));
	new->name = name;
	new->size = ft_strlen(name);
	if (new->size > env->size_max)
		env->size_max = new->size;
	if (begin)
		new->next = begin;
	else
		new->next = new;
	new->color = ft_init_color(name);
	return (new);
}

static void	ft_add_elem(t_select **begin, char *name, t_env *env)
{
	t_select *new;
	t_select *tmp;

	new = ft_new_elem(name, *begin, env);
	tmp = *begin;
	env->end = new;
	if (!tmp)
	{
		*begin = new;
		return ;
	}
	while (tmp->next != *begin)
		tmp = tmp->next;
	tmp->next = new;
}

t_select 	*ft_init_list(int argc , char **argv, t_env *env)
{
	t_select *select;

	select = (void *)0;
	if (argc == 1)
		return 0;
	++argv;
	env->size_max = 0;
	env->size_lst = 0;
	while (argc > 1)
	{
		ft_add_elem(&select, *argv++, env);
		env->size_lst++;
		--argc;
	}
	return (select);
}
