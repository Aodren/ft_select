#include "libft.h"
#include "ft_select.h"
#include <stdlib.h>
#include <sys/stat.h>

static char *ft_stat_color(struct stat *buf)
{
	mode_t type;

	type = buf->st_mode;
	//Foret de IF sur le type de fichier
	if (S_ISFIFO(type))
		return ft_strdup(COLOR_FIFO);
	if (S_ISSOCK(type))
		return ft_strdup(COLOR_FIFO);
	if (S_ISDIR(type))
		return ft_strdup(COLOR_DIR);
	return ft_strdup(COLOR_DEFAULT);
}

char	*ft_init_color(char *name)
{
	char *color;
	struct	stat *buf;

	if (!(buf = (struct stat*)ft_memalloc(sizeof(struct stat))))
		exit(EXIT_FAILURE);
	if (lstat(name, buf) == -1)
	{
		// Il faut mettre la couleur a blanc
		color = ft_strdup(COLOR_DEFAULT);
		free (buf);
		return (color);
	}
	color = ft_stat_color(buf);
	free (buf);
	buf = (void *)0;
	return (color);
}
