#include "ft_select.h"
#include <termios.h>
/*
 * definition de la struct termios emn mode cannonique
 */
static void	ft_define_new_term(struct termios* termios_p)
{
	termios_p->c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
			| INLCR | IGNCR | ICRNL | IXON);
	termios_p->c_oflag &= ~OPOST;
	termios_p->c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
	termios_p->c_cflag &= ~(CSIZE | PARENB);
	termios_p->c_cflag |= CS8;
}	

/*
 * Met en shell le mode canonique et remplie
 * la struct termios de la struct env avec l'ancienne confguration
 */

void ft_init_termios(t_env *env, int fd)
{
	struct termios termios_p;

	ft_define_new_term(&termios_p);
	//recuperation de lancienne config du terminal
	tcgetattr(fd, env->old_termios); // EN TRAVAUX
	//set le nouveau terminal
	tcsetattr(fd, TCSANOW, &termios_p);
}
