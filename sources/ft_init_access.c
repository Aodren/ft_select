#include "ft_select.h"
#include <stdlib.h>
#include <term.h>
/*
 * Garentie l'acces a la base de donnee des termcaps
 * Exit le programme si c'est impossible
 */

void	ft_init_access_db()
{
	char *env_term = (void *)0;
	int access_termdb;

	env_term = getenv("TERM");
	if (!env_term)
		ft_exit("Env variable TERM undefined", -1);
	access_termdb = tgetent((void *)0, env_term);
	if (access_termdb < 0)
		ft_exit("Could not access the databases", -1);
	if (access_termdb == 0)
		ft_exit ("terminal is not defined", -1 );
}
