#ifndef FT_SELECT_H
#define FT_SELECT_H

//#define DEFAULT	"\033[0;39;49m"
#define COLOR_DEFAULT	"\e[0;49m"
#define COLOR_FIFO	"\e[0;39;43m"
#define COLOR_DIR	"\e[1;34;49m"

// Faire une liste circulaire et doublement chainee ?????
typedef struct s_select
{
	char		*name;
	char		*color;
	// include un elem precdent
	unsigned long		size;
	struct s_select *next;
}		t_select;

typedef struct s_env
{
	t_select	*begin;
	t_select	*end;
	unsigned long		size_max; // taille u mot le plus grand
	unsigned long		size_lst; // taille de la liste
	struct	termios		*old_termios;
	int			width; // Largeur de la fenetre
	int			height; // Hauteur de la fenetre
}				t_env;


/*
 * Init
 */

void	ft_init_access_db(void);
/*************************
 * 	LISTE
 ************************/

t_select *ft_init_list(int argc, char **argv, t_env *env);
void		ft_init_env(t_env *env);
char	 *ft_init_color(char *name);
void	ft_init_env(t_env *env);

/**********************
 * 	Display
 ***********************/

int	ft_display_list(t_env *env, int *fd);

/***************
 * TTY
 **********/

int		ft_get_size_fenetre(t_env *env);
void		ft_init_termios(t_env *env, int fd);
/*
 *  TERM
 */
void	ft_manage_term(char *line, t_env *env);


void	ft_exit(char *message, int success);
#endif
