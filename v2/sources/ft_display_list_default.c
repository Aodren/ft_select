/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_list_default.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 11:56:52 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 17:15:11 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include "libft.h"

static int	ft_check_select(t_select *begin, t_env *env)
{
	t_select	*tmp;

	tmp = begin;
	while (tmp)
	{
		if (IS_SELECT(tmp->focus))
			return (0);
		tmp = tmp->next;
		if (begin == tmp)
			break ;
	}
	ft_quit(env);
	return (1);
}

void		ft_display_list_default(t_select *begin, t_env *env)
{
	t_select *tmp;

	if (ft_check_select(begin, env))
		return ;
	tmp = begin;
	while (begin)
	{
		if (IS_SELECT(begin->focus))
		{
			ft_putstr(begin->name);
			ft_putchar(' ');
		}
		begin = begin->next;
		if (begin == tmp)
			break ;
	}
}
