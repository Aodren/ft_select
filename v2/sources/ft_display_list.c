/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_list.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 12:20:32 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include "libft.h"

static void	ft_display_name(t_select *select, t_env *env, unsigned int col)
{
	unsigned int space;

	if (select->color)
		ft_putstr_fd(select->color, env->fd);
	else
		ft_putstr_fd(COLOR_DEFAULT, env->fd);
	if (IS_SELECT(select->focus))
		ft_term_highlight_on();
	if (IS_FOCUS(select->focus))
		ft_term_underling_on();
	ft_putstr_fd(select->name, env->fd);
	if (IS_FOCUS(select->focus))
		ft_term_underling_off();
	if (IS_SELECT(select->focus))
		ft_term_highlight_off();
	ft_putstr_fd(COLOR_DEFAULT, env->fd);
	space = select->size;
	while (space < env->padding[col])
	{
		ft_putchar_fd(' ', env->fd);
		++space;
	}
	ft_putchar_fd(' ', env->fd);
}

static int	ft_max_col(t_env *env)
{
	if (env->s_max >= env->col)
	{
		ft_putendl("...\n");
		return (1);
	}
	return (0);
}

static void	ft_display_end_line(t_select *select, t_env *env)
{
	if (select == env->begin)
	{
		ft_term_clear_end_line();
		ft_putstr_fd("\n   ", env->fd);
	}
}

static int	ft_display_end(t_select *select, t_env *env)
{
	if (select != env->end)
		ft_putstr_fd("\n...", env->fd);
	else
		ft_putstr_fd("\n   ", env->fd);
	return (1);
}

void		ft_display_list(t_env *env)
{
	unsigned int	nbr;
	unsigned int	col;
	t_select		*select;

	nbr = 0;
	col = 0;
	if (ft_max_col(env))
		return ;
	select = ft_skip_index(env, &nbr);
	while (nbr < env->s_list)
	{
		ft_display_name(select, env, col++);
		++nbr;
		if (col >= env->nbr_col)
		{
			col = 0;
			if (++env->nbr_ligne >= env->ligne - 1)
			{
				if (ft_display_end(select, env))
					break ;
			}
			ft_putstr_fd("\n", env->fd);
		}
		ft_display_end_line(select = select->next, env);
	}
}
