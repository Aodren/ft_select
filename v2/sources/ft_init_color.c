/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_color.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 14:24:27 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_select.h"
#include <sys/stat.h>

static char	*ft_stat_color(struct stat *buf)
{
	mode_t type;

	type = buf->st_mode;
	if (S_ISREG(type))
	{
		if ((S_IXOTH & type) == S_IXOTH
				|| (S_IXGRP & type) == S_IXGRP
				|| (S_IXUSR & type) == S_IXUSR)
			return (ft_strdup(COLOR_EXE));
	}
	if (S_ISDIR(type))
		return (ft_strdup(COLOR_DIR));
	else if (S_ISCHR(type))
		return (ft_strdup(COLOR_DEVICE));
	else if (S_ISBLK(type))
		return (ft_strdup(COLOR_BLOCK));
	else if (S_ISFIFO(type))
		return (ft_strdup(COLOR_FIFO));
	else if (S_ISLNK(type))
		return (ft_strdup(COLOR_LINK));
	else if (S_ISSOCK(type))
		return (ft_strdup(COLOR_SOCK));
	return ((void *)0);
}

char		*ft_init_color(char *name)
{
	struct stat buf;
	char		*color;

	if (lstat(name, &buf) == -1)
		return ((void *)0);
	color = ft_stat_color(&buf);
	return (color);
}
