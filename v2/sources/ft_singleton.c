/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_singleton.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 13:25:22 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include "libft.h"

t_env	*ft_singleton_env(void)
{
	static t_env *env = (void *)0;

	if (env == (void *)0)
		env = ft_init_env(env);
	return (env);
}
