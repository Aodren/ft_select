/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_acces_db.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 14:32:38 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include <stdlib.h>
#include <term.h>

void	ft_init_access_db(void)
{
	char	*env_term;
	int		access_termdb;

	env_term = (void *)0;
	env_term = getenv("TERM");
	if (!env_term)
		ft_display_error(ENV_TERM, 1);
	access_termdb = tgetent((void *)0, env_term);
	if (access_termdb < 0)
		ft_display_error(DATABASE_FOUND, 1);
	if (access_termdb == 0)
		ft_display_error(UNDEFINED_TERM, 1);
}
