/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_input.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 17:14:19 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_select.h"
#include <unistd.h>

static int	ft_return_arrow(char *str)
{
	if (*str == 0x41)
		return (UP);
	if (*str == 0x42)
		return (DOWN);
	if (*str == 0x44)
		return (LEFT);
	if (*str == 0x43)
		return (RIGHT);
	return (INVALID);
}

static int	ft_get_key(char *str)
{
	if (*str == 27)
	{
		++str;
		if (!*str)
			return (ESC);
		if (*str == 0x5b)
		{
			++str;
			if (ft_return_arrow(str) != INVALID)
				return (ft_return_arrow(str));
			--str;
		}
		--str;
	}
	if (*str == 0x0d && !*(str + 1))
		return (END);
	if (*str == 0x20 && !*(str + 1))
		return (SELECT);
	if (*str == 0x7f && !*(str + 1))
		return (DELETE);
	if (*str == 0x1b && *(str + 1) == 0x5b &&
			*(str + 2) == 0x33 && *(str + 3) == 0x7e
			&& !*(str + 4))
		return (DELETE);
	return (INVALID);
}

int			ft_read_input(t_env *env)
{
	int		ret;
	char	*d;

	d = env->buf;
	while (*env->buf)
		env->buf++;
	ret = read(0, env->buf, BUF_SIZE - (env->buf - d));
	if (ret > 0)
	{
		ret = ft_get_key(d);
		if (ret == ESC)
			ft_quit(env);
		return (ret);
	}
	env->buf = d;
	return (-1);
}
