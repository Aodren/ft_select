/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal_resize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 13:24:59 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include "ft_select.h"
#include "libft.h"

/*
** RESIZE DE LA FENETRE
*/

void	ft_sig_resize(int sig)
{
	t_env *env;

	env = (void *)0;
	if (sig == SIGWINCH)
	{
		env = ft_singleton_env();
		ft_term_cursor_home();
		ft_term_clear_screen();
		ft_calcul_size_fenetre(env);
		ft_calcul_col_ligne(env);
		ft_find_focus(env);
		ft_display_list(env);
	}
}
