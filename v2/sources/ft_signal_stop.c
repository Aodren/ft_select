/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal_stop.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 15:10:44 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_select.h"
#include <sys/ioctl.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void	ft_sig_stop(int sig)
{
	char	fg;
	t_env	*env;

	env = ft_singleton_env();
	ft_term_clear_screen();
	ft_term_cursor_home();
	ft_set_old_term(env);
	ft_term_cursor_normal();
	fg = env->old_termios.c_cc[VSUSP];
	close(env->fd);
	if (ioctl(0, TIOCSTI, &fg) == -1)
		ft_putendl("error");
	else
		signal(sig, SIG_DFL);
}

void	ft_sig_continue(int sig)
{
	t_env *env;

	(void)sig;
	env = ft_singleton_env();
	env->fd = -1;
	signal(SIGTSTP, ft_sig_stop);
	ft_calcul_size_fenetre(env);
	ft_get_old_term(env);
	ft_set_new_term(env);
	ft_term_cursor_invisible();
	ft_term_cursor_home();
	ft_term_clear_screen();
	ft_calcul_col_ligne(env);
	ft_find_focus(env);
	ft_display_list(env);
}
