/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_list.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 14:31:33 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include "libft.h"

static void	ft_add_elem(char *name, t_env *env)
{
	t_select *new;

	new = ft_init_list(name);
	if (new->size > env->s_max)
		env->s_max = new->size;
	if (!env->begin)
	{
		env->begin = new;
		env->end = new;
		new->next = new;
		new->prev = new;
		new->focus = 1;
		env->focus = new;
	}
	else
	{
		new->next = env->begin;
		new->prev = env->end;
		env->end->next = new;
		env->end = new;
		env->begin->prev = env->end;
	}
}

void		ft_create_list(t_env *env, int argc, char **argv)
{
	++argv;
	env->s_list = argc - 1;
	while (argc > 1)
	{
		ft_add_elem(*argv++, env);
		--argc;
	}
}
