/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_error.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 14:27:58 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include "libft.h"
#include <stdlib.h>

void	ft_display_error(int error, int success)
{
	if (error == PARSING)
		ft_putendl_fd("No arg is given, abording ...", 2);
	else if (error == MALLOC)
		ft_putendl_fd("Malloc error ...", 2);
	else if (error == ENV_TERM)
		ft_putendl_fd("TERM variable undefined", 2);
	else if (error == UNDEFINED_TERM)
		ft_putendl_fd("terminal is not defined", 2);
	else if (error == DATABASE_FOUND)
		ft_putendl_fd("Could not acces the database", 2);
	else if (error == TERMIOS_ERROR)
		ft_putendl_fd("Error termios", 2);
	if (success == 1)
		exit(EXIT_SUCCESS);
	else
		exit(EXIT_FAILURE);
}
