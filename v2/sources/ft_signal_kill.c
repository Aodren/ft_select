/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal_kill.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 17:17:35 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_select.h"
#include "libft.h"
#include <signal.h>

void	ft_sig_kill(int sig)
{
	t_env *env;

	if (sig == SIGINT || sig == SIGKILL || sig == SIGTERM)
	{
		env = (void *)0;
		env = ft_singleton_env();
		ft_destruct_all(env);
		exit(EXIT_SUCCESS);
	}
}
