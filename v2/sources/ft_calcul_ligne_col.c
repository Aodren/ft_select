/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calcul_ligne_col.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 14:27:22 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_select.h"

static void		ft_parcours_liste(t_env *env)
{
	unsigned int	nbr;
	unsigned int	col;
	t_select		*select;

	select = env->begin;
	nbr = 0;
	col = 0;
	while (nbr < env->s_list)
	{
		if (env->padding[col] < select->size)
			env->padding[col] = select->size;
		++col;
		++nbr;
		if (col >= env->nbr_col)
			col = 0;
		select = select->next;
	}
}

static void		ft_calcul_x_max(t_env *env, unsigned int *padding)
{
	env->x_max = 0;
	while (*padding != 0)
	{
		++env->x_max;
		++padding;
	}
	env->index_max = env->s_list / env->x_max;
	env->index_max = env->s_list % env->x_max ? env->index_max + 1
		: env->index_max;
}

/*
** calcul le nbr de col et de ligne a affiche
** ne pas faire comme le ls trop complique
*/

void			ft_calcul_col_ligne(t_env *env)
{
	env->nbr_col = env->col / (env->s_max + 1);
	env->nbr_ligne = 0;
	if (env->nbr_col > MAX_COL)
		env->nbr_col = MAX_COL;
	ft_bzero(env->padding, MAX_COL);
	env->index_max = 0;
	ft_parcours_liste(env);
	ft_calcul_x_max(env, env->padding);
}
