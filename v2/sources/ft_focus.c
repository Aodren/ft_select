/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_focus.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 17:20:56 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include "libft.h"

int	ft_focus_right(t_env *env)
{
	env->focus->focus ^= 1;
	env->focus->next->focus ^= 1;
	env->focus = env->focus->next;
	ft_find_focus(env);
	return (1);
}

int	ft_focus_left(t_env *env)
{
	env->focus->focus ^= 1;
	env->focus->prev->focus ^= 1;
	env->focus = env->focus->prev;
	ft_find_focus(env);
	return (1);
}

int	ft_focus_down(t_env *env)
{
	unsigned int	nbr;

	nbr = 0;
	env->focus->focus ^= 1;
	while (nbr < env->nbr_col)
	{
		env->focus = env->focus->next;
		++nbr;
	}
	env->focus->focus ^= 1;
	ft_find_focus(env);
	return (1);
}

int	ft_focus_up(t_env *env)
{
	unsigned int	nbr;

	nbr = 0;
	env->focus->focus ^= 1;
	while (nbr < env->nbr_col)
	{
		env->focus = env->focus->prev;
		++nbr;
	}
	env->focus->focus ^= 1;
	ft_find_focus(env);
	return (1);
}

int	ft_focus_select(t_env *env)
{
	env->focus->focus ^= 0b10;
	return (1);
}
