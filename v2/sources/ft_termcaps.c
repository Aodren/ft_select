/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 11:46:12 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <term.h>
#include "ft_select.h"

void	ft_term_highlight_on(void)
{
	char *ret;

	ret = (void *)0;
	if (!(ret = tgetstr("so", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_term_highlight_off(void)
{
	char *ret;

	ret = (void *)0;
	if (!(ret = tgetstr("se", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_term_underling_off(void)
{
	char *ret;

	ret = (void *)0;
	if (!(ret = tgetstr("ue", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_term_underling_on(void)
{
	char *ret;

	ret = (void *)0;
	if (!(ret = tgetstr("us", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}
