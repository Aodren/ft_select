/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calcul_size_fenetre.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 14:29:54 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include <sys/ioctl.h>
#include "libft.h"
#include <fcntl.h>
#include <unistd.h>

static int	ft_open_tty(t_env *env)
{
	char	*tty_name;

	if ((env->fd = ttyslot()) < 0)
	{
		ft_putendl("Erreur sur ttyslot");
		return (-1);
	}
	if (!isatty(0))
	{
		ft_putendl("Erreur sur isatty");
		return (-1);
	}
	if (!(tty_name = ttyname(0)))
		return (-1);
	if ((env->fd = open(tty_name, O_RDWR)) == -1)
	{
		ft_putendl("Erreur sur l'open du tty");
		return (-1);
	}
	return (env->fd);
}

void		ft_calcul_size_fenetre(t_env *env)
{
	struct winsize ws;

	if (env->fd == -1)
	{
		if ((env->fd = ft_open_tty(env)) == -1)
			ft_putendl("Erreur dnas la fonction open_tty");
		else if ((ioctl(env->fd, TIOCGWINSZ, &ws) == -1))
			ft_putendl("Erreur sur l'ioctl");
	}
	else if (env->fd != -1)
	{
		if ((ioctl(env->fd, TIOCGWINSZ, &ws) == -1))
			ft_putendl("Erreur sur l'ioctl");
	}
	env->col = ws.ws_col;
	env->ligne = ws.ws_row;
}
