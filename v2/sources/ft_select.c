/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 17:20:24 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_select.h"

void	ft_select(t_env *env)
{
	int ret;

	while ((ret = ft_read_input(env)))
	{
		if (ret > 0 && ret != INVALID)
		{
			if ((ret = env->ptab[ret](env)))
			{
				ft_term_cursor_home();
				if (ret == END)
				{
					ft_quit(env);
					break ;
				}
				ft_calcul_col_ligne(env);
				if (ret == DELETE)
					ft_find_focus(env);
				ft_display_list(env);
			}
		}
		ft_bzero(env->buf, BUF_SIZE);
	}
}
