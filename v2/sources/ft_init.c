/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 13:55:32 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include "libft.h"

static void	ft_init_ptab(t_env *env)
{
	env->ptab[0] = (void *)0;
	env->ptab[RIGHT] = ft_focus_right;
	env->ptab[LEFT] = ft_focus_left;
	env->ptab[UP] = ft_focus_up;
	env->ptab[DOWN] = ft_focus_down;
	env->ptab[SELECT] = ft_focus_select;
	env->ptab[DELETE] = ft_delete_select;
	env->ptab[END] = ft_end;
}

t_env		*ft_init_env(t_env *env)
{
	if (!(env = (t_env *)ft_memalloc(sizeof(t_env))))
		ft_display_error(MALLOC, 0);
	env->begin = (void *)0;
	env->end = (void *)0;
	env->focus = (void *)0;
	env->s_list = 0;
	env->s_max = 0;
	env->col = 0;
	env->ligne = 0;
	env->index = 0;
	env->x = 0;
	env->x_max = 0;
	if (!(env->buf = ft_memalloc(sizeof(char ))))
		ft_display_error(MALLOC, 0);
	*(env->buf + BUF_SIZE) = 0;
	env->fd = -1;
	ft_init_ptab(env);
	return (env);
}

t_select	*ft_init_list(char *name)
{
	t_select *new;

	new = (void *)0;
	if (!(new = ft_memalloc(sizeof(t_select))))
		ft_display_error(MALLOC, 0);
	new->name = name;
	new->color = ft_init_color(name);
	new->next = (void *)0;
	new->prev = (void *)0;
	new->size = ft_strlen(name);
	new->focus = 0;
	return (new);
}
