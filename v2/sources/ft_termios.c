/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termios.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 11:48:04 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ioctl.h>
#include "ft_select.h"
#include <termios.h>

void	ft_get_old_term(t_env *env)
{
	int ret;

	ret = tcgetattr(0, &(*env).old_termios);
	if (ret == -1)
		ft_display_error(TERMIOS_ERROR, 0);
}

void	ft_set_new_term(t_env *env)
{
	int ret;

	ret = tcgetattr(0, &(*env).new_termios);
	if (ret == -1)
		ft_display_error(TERMIOS_ERROR, 0);
	env->new_termios.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
		| INLCR | IGNCR | ICRNL | IXON);
	env->new_termios.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN);
	env->new_termios.c_cflag &= ~(CSIZE | PARENB);
	env->new_termios.c_cflag |= CS8;
	env->new_termios.c_cc[VMIN] = 0;
	env->new_termios.c_cc[VTIME] = 0;
	ret = tcsetattr(env->fd, TCSANOW, &env->new_termios);
	if (ret == -1)
		ft_display_error(TERMIOS_ERROR, 0);
}

void	ft_set_old_term(t_env *env)
{
	int ret;

	ret = tcsetattr(0, TCSANOW, &env->old_termios);
	if (ret == -1)
		ft_display_error(TERMIOS_ERROR, 0);
}
