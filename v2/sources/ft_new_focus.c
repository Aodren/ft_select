/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_new_focus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 13:54:33 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void		ft_find_focus_one_row(t_env *env)
{
	t_select *select;

	select = env->begin;
	while (select != env->focus)
	{
		++env->index;
		select = select->next;
	}
}

void			ft_find_focus(t_env *env)
{
	t_select	*select;

	select = env->begin;
	env->index = 0;
	env->x = 0;
	if (env->nbr_col == 1)
	{
		ft_find_focus_one_row(env);
		return ;
	}
	while (select != env->focus)
	{
		env->x++;
		if (env->x == env->nbr_col)
		{
			env->x = 0;
			env->index++;
		}
		select = select->next;
	}
}

int				ft_focus_down_end(t_env *env)
{
	if (env->focus == env->begin)
	{
		env->focus = env->end;
		env->focus->focus ^= 1;
		ft_find_focus(env);
		return (1);
	}
	return (0);
}
