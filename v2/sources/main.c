/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 11:47:30 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int	main(int argc, char **argv)
{
	t_env *env;

	env = (void*)0;
	ft_pars_arg(argc);
	env = ft_singleton_env();
	ft_signal_handler();
	ft_init_access_db();
	ft_create_list(env, argc, argv);
	ft_calcul_size_fenetre(env);
	ft_get_old_term(env);
	ft_set_new_term(env);
	ft_term_clear_screen();
	ft_term_cursor_invisible();
	ft_calcul_col_ligne(env);
	ft_display_list(env);
	ft_select(env);
	ft_destruct_all(env);
	return (0);
}
