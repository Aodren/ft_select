/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_list.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 17:18:05 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_select.h"

int		ft_delete_select(t_env *env)
{
	t_select *select;

	select = env->focus;
	ft_term_clear_screen();
	if (select->color)
	{
		free(select->color);
		select->color = (void *)0;
	}
	select->next->prev = select->prev;
	select->prev->next = select->next;
	if (env->begin == select)
		env->begin = env->begin->next;
	if (env->end == select)
		env->end = env->end->next;
	env->focus->focus ^= 0b1;
	env->focus = env->focus->next;
	env->focus->focus ^= 0b1;
	free(select);
	select = (void *)0;
	env->s_list--;
	if (!env->s_list)
		return (END);
	return (1);
}
