/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_skip_index.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 12:11:22 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 13:10:06 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_select	*ft_skip_index(t_env *env, unsigned int *index)
{
	unsigned int	nbr;
	unsigned int	col;
	t_select		*select;

	select = env->begin;
	nbr = env->index;
	while (nbr >= env->ligne - 1)
	{
		col = 0;
		while (col < env->nbr_col)
		{
			select = select->next;
			++*index;
			++col;
		}
		--nbr;
	}
	env->nbr_ligne = 0;
	return (select);
}
