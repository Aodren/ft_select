/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_destruct_list.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 14:24:58 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include <stdlib.h>

static	void	ft_free_list(t_select *select, t_env *env)
{
	if (select->next != env->begin)
		ft_free_list(select->next, env);
	if (select->color)
	{
		free(select->color);
		select->color = NULL;
	}
	free(select);
	select = NULL;
}

void			ft_destruct_list(t_env *env)
{
	if (env->s_list)
		ft_free_list(env->begin, env);
	env->begin = NULL;
	env->focus = NULL;
	env->end = NULL;
	if (env->buf)
		free(env->buf);
	env->buf = NULL;
	free(env);
}
