/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps_screen.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 17:24:04 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 11:48:27 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <term.h>
#include "ft_select.h"

void	ft_term_clear_screen(void)
{
	char *ret;

	if (!(ret = tgetstr("cl", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_term_clear_end_line(void)
{
	char *ret;

	if (!(ret = tgetstr("ce", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_term_cursor_home(void)
{
	char *ret;

	ret = (void *)0;
	if (!(ret = tgetstr("ho", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_term_cursor_invisible(void)
{
	char *ret;

	ret = (void *)0;
	if (!(ret = tgetstr("vi", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_term_cursor_normal(void)
{
	char *ret;

	ret = (void *)0;
	if (!(ret = tgetstr("ve", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}
