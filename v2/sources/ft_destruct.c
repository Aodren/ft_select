/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_destruct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 15:17:06 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include <stdlib.h>
#include <unistd.h>

void	ft_destruct_all(t_env *env)
{
	ft_term_cursor_home();
	ft_term_clear_screen();
	ft_term_cursor_normal();
	ft_set_old_term(env);
	close(env->fd);
	ft_destruct_list(env);
}
