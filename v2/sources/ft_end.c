/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_end.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:45 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 17:15:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_select.h"
#include <unistd.h>
#include <sys/ioctl.h>

int		ft_end(t_env *env)
{
	ft_term_cursor_home();
	ft_term_clear_screen();
	ft_term_cursor_normal();
	ft_display_list_default(env->begin, env);
	close(env->fd);
	ft_destruct_list(env);
	exit(EXIT_SUCCESS);
	return (END);
}

void	ft_quit(t_env *env)
{
	char eof;

	eof = env->old_termios.c_cc[VINTR];
	ioctl(0, TIOCSTI, &eof);
}
