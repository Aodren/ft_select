/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 13:25:43 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include "ft_select.h"

/*
** Gestion des signaux
*/

void	ft_signal_handler(void)
{
	unsigned int sig;

	sig = 1;
	while (sig < 32)
	{
		if (sig == SIGWINCH)
			signal(sig, ft_sig_resize);
		if (sig == SIGINT || sig == SIGKILL || sig == SIGTERM)
			signal(sig, ft_sig_kill);
		if (sig == SIGCONT)
			signal(sig, ft_sig_continue);
		if (sig == SIGTSTP)
			signal(sig, ft_sig_stop);
		++sig;
	}
}
