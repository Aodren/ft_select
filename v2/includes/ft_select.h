/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 16:57:44 by abary             #+#    #+#             */
/*   Updated: 2016/07/24 17:15:34 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include "termios.h"

# define MAX_COL 62
# define BUF_SIZE 10

/*
** COLOR
*/

# define COLOR_DEFAULT   "\e[0;49m"
# define COLOR_EXE	"\e[1;92;49m"
# define COLOR_DIR       "\e[1;34;49m"
# define COLOR_DEVICE	"\e[1;93;40m"
# define COLOR_BLOCK	"\e[1;93;40m"
# define COLOR_FIFO      "\e[0;33;49m"
# define COLOR_LINK      "\e[1;96;49m"
# define COLOR_SOCK      "\e[1;35;49m"

/*
** ERROR DEFINE
*/

enum	e_error { PARSING, MALLOC, UNDEFINED_TERM, DATABASE_FOUND, ENV_TERM,
	TERMIOS_ERROR};

# define ESC 0
# define UP 1
# define RIGHT 2
# define LEFT 3
# define DOWN 4
# define INVALID 5
# define SELECT 6
# define END 7
# define DELETE 8
# define NBR_ORDRE 9

# define IS_FOCUS(x) x & 0b1 ? 1 : 0
# define IS_SELECT(x) x & 0b10 ? 1 : 0

/*
** Liste chaine circulaire doublement chaine des arguments passe en parametres
*/

typedef	struct	s_select
{
	char				*name;
	char				*color;
	unsigned char		focus;
	unsigned int		size;
	unsigned int		padding;
	struct s_select		*next;
	struct s_select		*prev;
}				t_select;

/*
** Structure de l'environnement
*/

typedef struct	s_env
{
	unsigned int	s_list;
	unsigned int	s_max;
	unsigned short	col;
	unsigned short	ligne;
	unsigned int	padding[MAX_COL];
	unsigned short	nbr_col;
	unsigned short	nbr_ligne;
	struct termios	old_termios;
	struct termios	new_termios;
	char			*buf;
	unsigned int	index;
	unsigned int	index_max;
	unsigned int	x;
	unsigned int	x_max;
	int				fd;
	int				(*ptab[NBR_ORDRE])(struct s_env *);
	t_select		*begin;
	t_select		*end;
	t_select		*focus;
}				t_env;

/*
*******************************************************************
** 				Parsing
*********************************************************************
*/

void			ft_pars_arg(int argc);
int				ft_read_input(t_env *env);
void			ft_select(t_env *env);

/*
*******************************************************************
**                            INIT/CALCUL
*******************************************************************
*/

t_env			*ft_init_env(t_env *env);
t_select		*ft_init_list(char *name);
void			ft_init_access_db();
void			ft_create_list(t_env *env, int argc, char **argv);
char			*ft_init_color(char *name);
void			ft_calcul_size_fenetre(t_env *env);
void			ft_calcul_col_ligne(t_env *env);
void			ft_find_focus(t_env *env);
t_env			*ft_singleton_env(void);

/*
*******************************************************************
** 				DISPLAY
*******************************************************************
*/

void			ft_display_error(int error, int success);
void			ft_display_list(t_env *env);
void			ft_display_list_default(t_select *begin, t_env *env);
t_select		*ft_skip_index(t_env *env, unsigned int *index);

/*
**********************************************
**          TERM
*************************************************
*/

int				ft_putchar_term(int c);
void			ft_term_clear_screen(void);
void			ft_term_cursor_home(void);
void			ft_term_cursor_invisible(void);
void			ft_term_highlight_on(void);
void			ft_term_highlight_off(void);
void			ft_term_underling_on(void);
void			ft_term_underling_off(void);
void			ft_term_cursor_normal(void);
void			ft_term_clear_end_line(void);

void			ft_get_old_term(t_env *env);
void			ft_set_new_term(t_env *env);
void			ft_set_old_term(t_env *env);

/*
** ORDRES
*/

int				ft_focus_right(t_env *env);
int				ft_focus_left(t_env *env);
int				ft_focus_up(t_env *env);
int				ft_focus_down(t_env *env);
int				ft_focus_select(t_env *env);
int				ft_delete_select(t_env *env);
int				ft_focus_down_end(t_env *env);
int				ft_end(t_env *env);
void			ft_quit(t_env *end);

/*
** SIGNAL
*/

void			ft_signal_handler(void);
void			ft_sig_resize(int sig);
void			ft_sig_kill(int sig);
void			ft_sig_stop(int sig);
void			ft_sig_continue(int sig);

/*
** DESTRUCT
*/

void			ft_destruct_list(t_env *env);
void			ft_destruct_all(t_env *env);
#endif
