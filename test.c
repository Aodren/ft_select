#include <stdio.h>
#include <stdlib.h>
#include <term.h>
#define term_buffer 0

init_terminal_data ()
{
	char *termtype = getenv ("TERM");
	int success;

	if (termtype == 0)
	{
		printf ("Specify a terminal type with `setenv TERM '.\n");
		exit(EXIT_FAILURE);
	}

	success = tgetent ((void *)0, termtype);
	if (success < 0)
	{
		printf ("Could not access the termcap data base.\n");
		exit(EXIT_FAILURE);
	}
	if (success == 0)
	{
		printf ("Terminal type `%s' is not defined.\n", termtype);
		exit(EXIT_FAILURE);
	}
}

#define BUFFADDR 0
interrogate_terminal()
{
	char *cl_string, *cm_string;
	int height;
	int width;
	int auto_wrap;

	char PC; //For tputs
	char *BC; // for tgoto
	char *UP;
	
	char *temp;
	/* Extract information we will use*/
	cl_string = tgetstr ("cl", BUFFADDR);
	printf ("cl_string : %s\n", cl_string);
	cm_string = tgetstr ("cm", BUFFADDR);
	printf ("cm_string : %s\n", cm_string);
	auto_wrap = tgetflag ("am");
	height = tgetnum("li");
	printf("height : %d\n", height);
	width = tgetnum("co");
	printf("width : %d\n", width);

	/*Extract information that termcap functions use */
	temp = tgetstr ("pc", BUFFADDR);
	PC = temp ? *temp : 0;
	BC = tgetstr ("le", BUFFADDR);
	UP = tgetstr ("up", BUFFADDR);
}


int main()
{
	init_terminal_data();
	interrogate_terminal();
	while (1);

}
